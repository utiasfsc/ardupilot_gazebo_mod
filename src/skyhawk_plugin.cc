/******************************************************************************/
/* Skyhawk Plugin for UTAT
/* 2018-04-01 Yih Tang Yeo
/* yihtang.yeo@flight.utias.utoronto.ca
/*
/* This plugin allows ROS to subscribe to the /gazebo_state topic to get position and 
/* attitude data.
/*
/******************************************************************************/

#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "std_msgs/Float32.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include "include/skyhawk_plugin.hh"

namespace gazebo
{
  // A plugin to control the Drone
  class SkyhawkPlugin : public ModelPlugin
  {
    // Constructor
    public: SkyhawkPlugin() {}

    // The load function is called by Gazebo when the plugin is
    // inserted into simulation
    //
    // _model A pointer to the model that this plugin is attached to
    // _sdf A pointer to the plugin's SDF element
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
    
      // Safety check
      if (_model->GetJointCount() == 0)
      {
        std::cerr << "Invalid joint count, plugin not loaded\n";
        return;
      }

      // Store the model pointer for convenience
      this->model = _model;
      
      // Keep old time so that a time difference can be computed in the future
      this->old_time = this->model->GetWorld()->GetSimTime();
                  
      // Initialize ros, if it has not already bee initialized
      if (!ros::isInitialized())
      {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "gazebo_client",
            ros::init_options::NoSigintHandler);
      }

      // Create our ROS node. This acts in a similar manner to the Gazebo node
      this->rosNode.reset(new ros::NodeHandle("gazebo_client"));
      
      // Create topic /gazebo_state, and publish position and orientation to it
      this->pose_publisher = this->rosNode->
        advertise<geometry_msgs::TransformStamped>("/" + 
        this->model->GetName() + "/gazebo_state", 300);
      
      // Spin up the queue helper thread
      this->rosQueueThread =
        std::thread(std::bind(&SkyhawkPlugin::QueueThread, this));
      
    }

    // ROS helper function that processes messages
    private: void QueueThread()
    {
      static const double timeout = 0.01;
      while (this->rosNode->ok())
      {
        this->rosQueue.callAvailable(ros::WallDuration(timeout));
	this->PublishGazeboState();
      }
    }

    // Publish position and attitude to /gazebo_state topic
    private: void PublishGazeboState()
    {
      // Get the pose from Gazebo
      this->drone_pose = this->model->GetWorldPose();
      
      // Put the position information into the GazeboState message
      this->gazebo_state_msg.transform.translation.x = this->drone_pose.pos.x;
      this->gazebo_state_msg.transform.translation.y = this->drone_pose.pos.y;
      this->gazebo_state_msg.transform.translation.z = this->drone_pose.pos.z;
      
      // Put the rotation information into the GazeboState message
      this->gazebo_state_msg.transform.rotation.x = this->drone_pose.rot.x;
      this->gazebo_state_msg.transform.rotation.y = this->drone_pose.rot.y;
      this->gazebo_state_msg.transform.rotation.z = this->drone_pose.rot.z;
      this->gazebo_state_msg.transform.rotation.w = this->drone_pose.rot.w;

      this->gazebo_state_msg.header.stamp.sec =  this->model->GetWorld()->GetSimTime().sec;
      this->gazebo_state_msg.header.stamp.nsec =  this->model->GetWorld()->GetSimTime().nsec;
      
      // Publish the GazeboState message containing the position and attitude
      this->pose_publisher.publish(this->gazebo_state_msg);
    }

    bool FindJoint(const std::string &_sdfParam, sdf::ElementPtr _sdf,
        physics::JointPtr &_joint)
    {
      // Read the required plugin parameters
      if (!_sdf->HasElement(_sdfParam))
      {
        gzerr << "Unable to find <" << _sdfParam << "> parameter." << std::endl;
        return false;
      }

      std::string jointName = _sdf->Get<std::string>(_sdfParam);
      _joint = this->model->GetJoint(jointName);
      if (!_joint)
      {
        gzerr << "Failed to find joint [" << jointName
              << "] aborting plugin load." << std::endl;
        return false;
      }
      return true;
    }

    // A node use for ROS transport
    private: std::unique_ptr<ros::NodeHandle> rosNode;

    // A ROS subscriber for propeller speeds
    private: ros::Subscriber prop_subscriber;

    // A ROS publisher for positions and attitude
    private: ros::Publisher pose_publisher;
    
    // A ROS callbackqueue that helps process messages
    private: ros::CallbackQueue rosQueue;

    // A thread that keeps running the rosQueue
    private: std::thread rosQueueThread;
    private: std::thread statePublishThread;	

    // Gazebo time for time difference calculations
    private: gazebo::common::Time old_time;

    // Pointer to the model
    private: physics::ModelPtr model;

    // Pointer to the joint
    private: physics::JointPtr joint;
    // Create a GazeboState message (custom)
    private: geometry_msgs::TransformStamped gazebo_state_msg;

    // Create a Gazebo Pose, for pose information
    private: gazebo::math::Pose drone_pose;
  };

  // Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin
  GZ_REGISTER_MODEL_PLUGIN(SkyhawkPlugin)
}

