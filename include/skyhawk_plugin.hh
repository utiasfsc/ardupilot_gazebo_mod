/******************************************************************************/
/* Skyhawk Plugin for UTAT
/* 2018-04-01 Yih Tang Yeo
/* yihtang.yeo@flight.utias.utoronto.ca
/*
/* This plugin allows ROS to subscribe to the /gazebo_state topic to get position and 
/* attitude data.
/*
/******************************************************************************/

#ifndef _DRONE_PLUGIN_HH_
#define _DRONE_PLUGIN_HH_

#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "std_msgs/Float32.h"

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include "skyhawk_plugin.hh"



#endif

